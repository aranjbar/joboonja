<%@ page pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="core.domain.*" %>
<%@ page isELIgnored="false" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>his/her info</title>
</head>
<body>
<ul>
    <li>id: <c:out value="${user.id}"/></li>
    <li>title: <c:out value="${user.getFirstName()}"/></li>
    <li>description: <c:out value="${user.getLastName()}"/></li>
    <li><img height="100px" src="${user.getImageUrl()}"/></li>
    <li>jobTitle: <c:out value="${user.getJobTitle()}"/></li>
    <li>bio: <c:out value="${user.getBio()}"/></li>
    <li>skills:
        <br/>
        <ul>
            <c:forEach var="skill" items="${user.getSkills()}">
                <li>
                    <c:out value="${skill.getName()}"/>:<c:out value="${skill.getPoint()}"/>
                    <form action="/api/user/skill/endorse" method="POST">
                        <button type="submit">Endorse</button>
                        <input type="hidden" name="endorserUserId" value="1">
                        <input type="hidden" name="endorsedUserId" value="${user.getId()}">
                        <input type="hidden" name="skill" value="${skill.getName()}">
                    </form>
                </li>
            </c:forEach>
        </ul>
    </li>
</ul>

</body>
</html>