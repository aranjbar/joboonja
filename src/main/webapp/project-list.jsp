<%@ page pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="core.domain.*" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Project List</title>
    <style>
        table {
            text-align: center;
            margin: 0 auto;
        }
        td {
            min-width: 300px;
            margin: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
</head>
<body>
<table border=1>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Budget</th>
    </tr>
    <c:forEach var="project" items="${projectList}">
        <tr>
            <c:set var="projectId" value="${project.id}" />
            <td><a href = "<c:url value = "/project"><c:param name="id" value="${projectId}"/></c:url>"><c:out value="${project.id}"/></a></td>
            <td><c:out value="${project.title}"/></td>
            <td><c:out value="${project.budget}"/></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>