<%@ page pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="core.domain.*" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Project</title>
</head>
<body>
    <table border=1>
        <ul>
            <li>id: <c:out value="${project.id}"/></li>
            <li>title: <c:out value="${project.getTitle()}"/></li>
            <li>description: <c:out value="${project.description}"/></li>
            <li>budget: <c:out value="${project.budget}"/></li>
        </ul>

        <img height="100px" src="${project.imageUrl}"/>

        <p>
            Bid for <c:out value="${project.getTitle()}"/>
        </p>
        <form action="/bid" method=POST>
            <input type="hidden" name="projectTitle" value="${project.getTitle()}"/>
            Bid Amount: <input type="number" name="bidAmount" value="${student.homework}"/><br/>
            <input type="submit" value="Save"/>
        </form>
    </table>
</body>
</html>