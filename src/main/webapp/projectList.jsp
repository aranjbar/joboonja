<%@ page pageEncoding="utf-8" contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@ page import="core.domain.Project" %>--%>
<%--<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%--<%@ page isELIgnored="false" %>--%>

<html>
<head>
    <title>Project List</title>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<%--<c:if test="${not empty msg}">--%>
    <%--<h1><c:out value="${msg}"/></h1>--%>
<%--</c:if>--%>
<table border=1>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Budget</th>
    </tr>
    <c:forEach var="project" items="${projectList}">
        <tr>
            <td><c:out value="${project.id}"/></td>
            <td dir="rtl"><c:out value="${project.getTitle()}"/></td>
            <td><c:out value="${project.budget}"/></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>