<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="core.domain.*" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<html>
<head>
    <meta charset="UTF-8">
    <title>User List</title>
    <style>
        table {
            text-align: center;
            margin: 0 auto;
        }
        td {
            min-width: 300px;
            margin: 5px 5px 5px 5px;
            text-align: center;
        }
    </style>
</head>
<body>
<table border=1>
    <tr>
        <th>ID</th>
        <th>First Name</th>
        <th>Job Title</th>
    </tr>
    <c:forEach var="user" items="${userList}">
        <tr>
            <c:set var="userId" value="${user.id}" />
            <td><c:out value="${user.id}"/></a></td>
            <td><c:out value="${user.firstName}"/></td>
            <td><c:out value="${user.jobTitle}"/></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>