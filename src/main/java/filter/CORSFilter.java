package filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//@WebFilter("/")
public class CORSFilter implements Filter {

    public CORSFilter () {}

    public void init(FilterConfig fConfig) throws ServletException {}

    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        System.out.println("CORSFilter HTTP Request: " + req.getMethod() + " " + req.getRequestURI());
        HttpServletResponse res = (HttpServletResponse) response;



        // Authorize (allow) all domains to consume the content
        ((HttpServletResponse) response).addHeader("User-Agent", "Mozila/5.0");
        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Origin", "*");
        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Headers", "Authorization, Content-type, endorser");
        ((HttpServletResponse) response).addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
        ((HttpServletRequest) request).setCharacterEncoding("UTF-8");
        ((HttpServletResponse) response).setCharacterEncoding("UTF-8");
        ((HttpServletResponse) response).addHeader("Content-Type", "application/json");

        // For HTTP OPTIONS verb/method reply with ACCEPTED status code -- per CORS handshake
        if (req.getMethod().equals("OPTIONS")) {
            res.setStatus(HttpServletResponse.SC_OK);
            return;
        }

        chain.doFilter(req, response);
    }

    public void destroy() {}
}
