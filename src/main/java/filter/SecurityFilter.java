package filter;

import core.domain.User;
import utils.JWT;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@WebFilter("/*")
public class SecurityFilter implements Filter {
    public SecurityFilter () {}

    public void init (FilterConfig fconfig) {}

    private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(new HashSet<>(
            Arrays.asList("", "/users/authenticate", "/users/register")));

    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        System.out.println("SecurityFilter HTTP Request: " + req.getMethod() + " " + req.getRequestURI());
        HttpServletResponse res = (HttpServletResponse) response;
        String path = req.getRequestURI().substring(req.getContextPath().length()).replaceAll("[/]+$", "");
        String token = req.getHeader("Authorization");

        boolean allowedPath = ALLOWED_PATHS.contains(path);
        if (allowedPath && token == null) {
            chain.doFilter(req, res);
        }
        else {
            boolean validToken = JWT.isValidToken(token);
            if (validToken) {
                User user = JWT.getPayload(token);
                req.setAttribute("user", user);
                chain.doFilter(req, res);
            }

            if (token == null) {
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            } else {
                res.setStatus(HttpServletResponse.SC_FORBIDDEN);
            }
        }
    }

    public void destroy() {}

}
