package dao;

import core.domain.Skill;
import core.domain.User;
import utils.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

// for now, the entire so called "Data Base" is stored in memory in DAO
public class UserDAO {
    private static ArrayList<User> userDB = new ArrayList<User>();

    public static void addUser(User user) {

        String insertUserQuery = "insert into users " +
                "(id, firstName, lastName, jobTitle, bio, imageUrl, password) values " +
                "(?, ?, ?, ?, ?, ?, ?)";

        try {

            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(insertUserQuery);
            pstmt.setString(1, user.getId());
            pstmt.setString(2, user.getFirstName());
            pstmt.setString(3, user.getLastName());
            pstmt.setString(4, user.getJobTitle());
            pstmt.setString(5, user.getBio());
            pstmt.setString(6, user.getImageUrl());
            pstmt.setString(7, user.getPassword());

            pstmt.executeUpdate();

            pstmt.close();

            // add skills
            for (Skill skill: user.getSkills()) {
                String insertSkillQuery = "insert into userSkills (userId, skillName, point) values " +
                        "(?, ?, ?)";

                pstmt = con.prepareStatement(insertSkillQuery);
                pstmt.setString(1, user.getId());
                pstmt.setString(2, skill.getName());
                pstmt.setInt(3, skill.getPoint());

                pstmt.executeUpdate();

            }

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            // some error, or maybe the user already exists
            System.out.println("Error in UserDAO::addUser");
            System.out.println(insertUserQuery);
        }

    }
    
    public static User retrieveUserByID (String id) {

        User result = null;

        try {
            Connection con = ConnectionPool.getConnection();

            String userQuery = "select * from users where id = ?";
            String userSkillsQuery = "select skillName, point from userSkills where userId = ?";

            PreparedStatement pstmUsers = con.prepareStatement(userQuery);
            pstmUsers.setString(1, id);

            PreparedStatement pstmSkills = con.prepareStatement(userSkillsQuery);
            pstmSkills.setString(1, id);


            ResultSet userRs = pstmUsers.executeQuery();
            ResultSet skillsRs = pstmSkills.executeQuery();

            while (userRs.next()) {

                String _id = userRs.getString("id");
                String _first = userRs.getString("firstName");
                String _last = userRs.getString("lastName");
                String _job = userRs.getString("jobTitle");
                String _bio = userRs.getString("bio");
                String _imgUrl = userRs.getString("imageUrl");
                String _pass = userRs.getString("password");


                result = new User(_id, _first, _last, _job, _bio, _imgUrl, _pass);
            }

            if (result != null) {
                ArrayList<Skill> userSkills = new ArrayList<>();
                while (skillsRs.next()) {
                    String _skillName = skillsRs.getString("skillName");
                    Integer _point = skillsRs.getInt("point");
                    userSkills.add(new Skill(_skillName, _point));
                }

                result.setSkills(userSkills);
            }

            pstmUsers.close();
            pstmSkills.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in UserDAO::retrieveUserById");
        }

        return result;
    }

    // when someone adds a skill to itself
    public static void addSkill(String id, Skill skill) {

        String insertSkillQuery = "insert into userSkills " +
                "(userId, skillName, point) values " +
                "(?, ?, ?)";

        try {

            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(insertSkillQuery);
            pstmt.setString(1, id);
            pstmt.setString(2, skill.getName());
            pstmt.setInt(3, 0);

            pstmt.executeUpdate();

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in UserDAO::addSkill");
        }
    }

    public static ArrayList<User> retrieveOtherUsersList(String selfUserId) {
        ArrayList<User> result = new ArrayList<>();
        String userListQuery = "select * from users where id <> " + "\"" + selfUserId + "\"";
        try {

            Connection con = ConnectionPool.getConnection();

            PreparedStatement pstmt = con.prepareStatement(userListQuery);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String _id = rs.getString("id");
                String _first = rs.getString("firstName");
                String _last = rs.getString("lastName");
                String _job = rs.getString("jobTitle");
                String _bio = rs.getString("bio");
                String _imgUrl = rs.getString("imageUrl");
                String _pass = rs.getString("password");

                result.add(new User(_id, _first, _last, _job, _bio, _imgUrl, _pass));
            }

            pstmt.close();
            con.close();


        } catch (SQLException e) {
            System.out.println("Error in UserDAO::retrieveOtherUsersList");
        }

        return result;
    }

    public static ArrayList<User> searchUser(String wildcard) {
        String searchQuery = "select id from users where firstName like \'?\' OR "
                + "lastName like \'?\'";
        ArrayList<User> result = new ArrayList<>();

        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(searchQuery);
            pstmt.setString(1, wildcard);
            pstmt.setString(2, wildcard);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String _id = rs.getString("id");

                User u = UserDAO.retrieveUserByID(_id);
                result.add(u);
            }


        } catch (SQLException e) {
            System.out.println("Error in UserDAO::searchUser");
        }

        return result;
    }

}
