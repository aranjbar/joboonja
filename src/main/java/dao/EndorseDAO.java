package dao;

import core.domain.Endorse;
import core.exception.EndorseAlreadyExists;
import utils.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EndorseDAO {
    private static ArrayList<Endorse> endorseDB = new ArrayList<>();

    public static void addEndorse(String endorserUserId, String endorsedUserId, String skillName) throws EndorseAlreadyExists {
        if (retrieveEndorse(endorserUserId, endorsedUserId, skillName)) {
            throw new EndorseAlreadyExists();
        } else {


            String insertEndorseQuery = "insert into endorses (endorserId, endorsedId, skillName) values " +
                    "(?, ?, ?)";


            String updateUserSkillPointQuery = "update userSkills set point = point + 1 where " +
                    "userId == ? AND skillName == ?";

            try {
                Connection con = ConnectionPool.getConnection();

                PreparedStatement pstmt = con.prepareStatement(insertEndorseQuery);
                pstmt.setString(1, endorserUserId);
                pstmt.setString(2, endorsedUserId);
                pstmt.setString(3, skillName);


                pstmt.executeUpdate();
                pstmt.close();

                pstmt = con.prepareStatement(updateUserSkillPointQuery);
                pstmt.setString(1, endorsedUserId);
                pstmt.setString(2, skillName);

                pstmt.executeUpdate();

                pstmt.close();
                con.close();

            } catch (SQLException e) {
                System.out.println("Error in EndorseDAO::addEndorse");
            }

        }
    }

    private static boolean retrieveEndorse(String endorserUserId, String endorsedUserId, String skillName) {

        String selectEndorseQuery = "select * from endorses where " +
                "endorserId == ? AND " +
                "endorsedId == ? AND " +
                "skillName == ? ";

        Boolean result = false;

        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(selectEndorseQuery);
            pstmt.setString(1, endorserUserId);
            pstmt.setString(2, endorsedUserId);
            pstmt.setString(3, skillName);


            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                result = true;
            }

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in EndorseDAO::retrieveEndorse");
        }

        return result;

    }

    public static ArrayList<String> retrieveUserEndorses(String endorserUserId, String endorsedUserId) {
        ArrayList<String> skills = new ArrayList<>();

        String selectEndorseQuery = "select skillName from endorses where " +
                "endorserId == ? AND " +
                "endorsedId == ? ";

        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(selectEndorseQuery);
            pstmt.setString(1, endorserUserId);
            pstmt.setString(2, endorsedUserId);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                skills.add(rs.getString("skillName"));
            }

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in EndorseDAO::retrieveUserEndorses");
        }


        return skills;
    }
}
