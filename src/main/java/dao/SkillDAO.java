package dao;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import core.domain.Skill;
import utils.ConnectionPool;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SkillDAO {
    private static ArrayList<Skill> skillsDB = new ArrayList<>();

    public static void addSkillBulk(String skillsListString) {
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Skill>>(){}.getType();
        try {
            ArrayList<Skill> skillsList = gson.fromJson(new InputStreamReader(new ByteArrayInputStream(skillsListString.getBytes()), "UTF-8"), listType);

            try {
                for (Skill skill: skillsList) {
                    String insertSkillQuery = "insert into skills (name) values (?)";

                    Connection con = ConnectionPool.getConnection();
                    PreparedStatement pstmt = con.prepareStatement(insertSkillQuery);
                    pstmt.setString(1, skill.getName());

                    pstmt.executeUpdate();

                    pstmt.close();
                    con.close();

                }
            } catch (SQLException e) {
                System.out.println("Error in SkillDAO::addSkillBulk");
            }

        } catch (UnsupportedEncodingException e) {}
    }

    public static ArrayList<Skill> subtractSkillArray(ArrayList<Skill> skills) {
        ArrayList<Skill> result = new ArrayList<>();

        String selectSkillsQuery = "select * from skills where ";
        for (int i = 0; i < skills.size() - 1; i++) {
            selectSkillsQuery = selectSkillsQuery + "name <> " + "\"" + skills.get(i).getName() + "\" AND ";
        }
        selectSkillsQuery = selectSkillsQuery + "name <> " + "\"" + skills.get(skills.size() - 1).getName() + "\"";

        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(selectSkillsQuery);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String _name = rs.getString("name");
                result.add(new Skill(_name, 0));
            }

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in SkillDAO::subtractSkillArray");
        }


        return result;
    }
}
