package dao;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import core.domain.Bid;
import core.domain.Project;
import core.domain.Skill;
import core.validator.BidValidator;
import utils.ConnectionPool;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProjectDAO {
    private static ArrayList<Project> projectDB = new ArrayList<Project>();

    public static void addProject(Project project) {

        String insertProjectQuery = "insert into projects " +
                "(id, title, description, imageUrl, budget, deadline, creationDate) values " +
                "(?, ?, ?, ?, ?, ?, ?)";

        try {

            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(insertProjectQuery);

            pstmt.setString(1, project.getId());
            pstmt.setString(2, project.getTitle());
            pstmt.setString(3, project.getDescription());
            pstmt.setString(4, project.getImageUrl());
            pstmt.setInt(5, project.getBudget());
            pstmt.setLong(6, project.getDeadline());
            pstmt.setLong(7, project.getCreationDate());

            pstmt.executeUpdate();

            // add skills
            for (Skill skill: project.getSkills()) {

                String insertSkillQuery = "insert into projectSkills (projectId, skillName, point) values " +
                        "(?, ?, ?)";

                pstmt = con.prepareStatement(insertSkillQuery);
                pstmt.setString(1, project.getId());
                pstmt.setString(2, skill.getName());
                pstmt.setInt(3, skill.getPoint());

                pstmt.executeUpdate();
            }

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            // maybe there was a duplicate project
//            System.out.println("Error in ProjectDAO::addProject");
//            System.out.println(insertProjectQuery);
        }




    }

    public static Project retrieveProjectById(String id) {
        Project result = null;

        try {
            Connection con = ConnectionPool.getConnection();

            String projectQuery = "select * from projects where id = ?";
            String projectSkillsQuery = "select skillName, point from projectSkills where projectId = ?";
            String projectBidsQuery = "select bidderId, amount from bids where projectId = ?";


            PreparedStatement pstmProject = con.prepareStatement(projectQuery);
            pstmProject.setString(1, id);


            PreparedStatement pstmSkills = con.prepareStatement(projectSkillsQuery);
            pstmSkills.setString(1, id);

            PreparedStatement pstmBids = con.prepareStatement(projectBidsQuery);
            pstmBids.setString(1, id);


            ResultSet projectRs = pstmProject.executeQuery();
            ResultSet skillsRs = pstmSkills.executeQuery();
            ResultSet bidsRs = pstmBids.executeQuery();

            while (projectRs.next()) {

                String _id = projectRs.getString("id");
                String _title = projectRs.getString("title");
                String _desc = projectRs.getString("description");
                String _imgUrl = projectRs.getString("imageUrl");
                Integer _budget = projectRs.getInt("budget");
                Long _deadline = projectRs.getLong("deadline");
                Long _creData = projectRs.getLong("creationDate");



                result = new Project(_title, _budget, _id, _desc, _imgUrl, _deadline, _creData);
            }

            if (result != null) {
                ArrayList<Skill> projectSkills = new ArrayList<>();
                while (skillsRs.next()) {
                    String _skillName = skillsRs.getString("skillName");
                    Integer _point = skillsRs.getInt("point");
                    projectSkills.add(new Skill(_skillName, _point));
                }

                result.setSkills(projectSkills);


                ArrayList<Bid> bids = new ArrayList<>();
                while (bidsRs.next()) {
                    String _bidderId = bidsRs.getString("bidderId");
                    Integer _amount = bidsRs.getInt("amount");
                    bids.add(new Bid(_bidderId, id, _amount));
                }

                result.setBids(bids);

            }

            pstmProject.close();
            pstmSkills.close();
            pstmBids.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in ProjectDAO::retrieveProjectById");
            System.out.println(e.getMessage());
        }

        return result;

    }

    public static void addProjectBulk(String projectListString) {
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Project>>(){}.getType();
        // Convert encoding to UTF-8
        try {
            ArrayList<Project> projectList = gson.fromJson(new InputStreamReader(new ByteArrayInputStream(projectListString.getBytes()), "UTF-8"), listType);
            for (Project p:projectList) {
                addProject(p);
            }
        } catch (UnsupportedEncodingException e) {}
    }

    public static ArrayList<Project> retrieveProjectList() {
        return projectDB;
    }


    // to be re-implemented
    public static ArrayList<Project> retrieveQualifiedProjectList(ArrayList<Skill> skills) {
        ArrayList<Project> qualifiedProjects = new ArrayList<>();
        for (Project project: projectDB) {
            if (BidValidator.qualifyUserForProject(skills, project.getSkills())) {
                qualifiedProjects.add(project);
            }
        }
        return qualifiedProjects;
    }

    public static ArrayList<Project> retrieveAllProjects(Integer itemsPerPage, Integer pageNumber) {

        String allProjectsQuery = "select id from projects order by deadline desc limit ? offset ?";
        ArrayList<Project> result = new ArrayList<>();

        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(allProjectsQuery);
            pstmt.setInt(1, itemsPerPage);
            pstmt.setInt(2, (pageNumber - 1) * itemsPerPage);

            ResultSet projectRs = pstmt.executeQuery();

            while (projectRs.next()) {

                String _id = projectRs.getString("id");
                Project p = ProjectDAO.retrieveProjectById(_id);

                result.add(p);
            }

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in ProjectDAO::retrieveAllProjects");
        }

        return result;
    }


    public static Bid retrieveBid(String bidderId, String projectId) {

        Bid result = null;
        String selectBidQuery = "select amount from bids where bidderId == ? AND projectId == ?";


        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(selectBidQuery);
            pstmt.setString(1, bidderId);
            pstmt.setString(2, projectId);

            ResultSet rs = pstmt.executeQuery();

            while(rs.next()) {
                Integer _amount = rs.getInt("amount");

                result = new Bid(bidderId, projectId, _amount);
            }

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in ProjectDAO::retrieveBid");
        }

        return result;
    }

    public static void insertBid(Bid bid) {

        String insertBidQuery = "insert into bids (bidderId, projectId, amount) values " +
                "(?, ?, ?)";

        try {

            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(insertBidQuery);
            pstmt.setString(1, bid.getBiddingUserId());
            pstmt.setString(2, bid.getProjectId());
            pstmt.setInt(3, bid.getBidAmount());

            pstmt.executeUpdate();

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in ProjectDAO::insertBid");
        }
    }

    public static ArrayList<Project> searchProject(String wildcard) {
        String searchQuery = "select id from projects where title like " + "\'?\'" +
                " or description like " + "\'?\'";

        ArrayList<Project> result = new ArrayList<>();

        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(searchQuery);
            pstmt.setString(1, wildcard);
            pstmt.setString(2, wildcard);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String _id = rs.getString("id");

                Project p = ProjectDAO.retrieveProjectById(_id);
                result.add(p);
            }


        } catch (SQLException e) {
            System.out.println("Error in ProjectDAO::searchProject");
        }

        return result;
    }

    public static void terminateProjectAuctionById(String projectId, String winnerUserId) {
        if (winnerUserId == null) {
            // this project had no winner at all
            return;
        }
        // add this user to winnerUsers table

        String insertWinnerQuery = "insert into winnerUsers (winnerId, projectId) values " +
                "(?, ?)";

        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(insertWinnerQuery);

            pstmt.setString(1, winnerUserId);
            pstmt.setString(2, projectId);

            pstmt.executeUpdate();

            pstmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in ProjectDAO::terminateProjectAuctionById");
        }

    }

}
