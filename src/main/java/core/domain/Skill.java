package core.domain;

import java.util.ArrayList;

public class Skill {
    private String name;
    private Integer point;

    public Skill() { }

    public Skill(String name, Integer point) {
        this.name = name;
        this.point = point;
    }

    public void incrementPoint() { this.point++; }

    public void setName(String name) {
        this.name = name;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getName() {
        return this.name;
    }

    public Integer getPoint() {
        return this.point;
    }

    public Boolean in(ArrayList<Skill> skills) {
        for (Skill skill: skills) {
            if (skill.getName().equals(this.name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Skill: {" +
                "name='" + name + '\'' +
                ", point=" + point +
                '}';
    }
}
