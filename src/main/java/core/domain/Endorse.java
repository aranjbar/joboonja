package core.domain;

public class Endorse {
    public String getEndorserUserId() {
        return endorserUserId;
    }

    public String getEndorsedUserId() {
        return endorsedUserId;
    }

    public String getSkillName() {
        return skillName;
    }

    private String endorserUserId, endorsedUserId, skillName;

    public Endorse(String endorserUserId, String endorsedUserId, String skillName) {
        this.endorserUserId = endorserUserId;
        this.endorsedUserId = endorsedUserId;
        this.skillName = skillName;
    }
}
