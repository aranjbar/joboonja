package core.domain;

public class Bid {

    private String biddingUserId;
    private String projectId;
    private Integer bidAmount;

    public Bid() {
        this.biddingUserId = null;
        this.projectId = null;
        this.bidAmount = null;
    }

    public Bid(String biddingUserId, String projectId, Integer bidAmount) {
        this.biddingUserId = biddingUserId;
        this.projectId = projectId;
        this.bidAmount = bidAmount;
    }



    public String getBiddingUserId() {
        return biddingUserId;
    }

    public void setBiddingUserId(String biddingUserId) {
        this.biddingUserId = biddingUserId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Integer getBidAmount() {
        return bidAmount;
    }

    public void setBidAmount(Integer bidAmount) {
        this.bidAmount = bidAmount;
    }



    @Override
    public String toString() {
        return "Bid{" +
                "biddingUserId='" + biddingUserId + '\'' +
                ", projectTitle='" + projectId + '\'' +
                ", bidAmount=" + bidAmount +
                '}';
    }
}
