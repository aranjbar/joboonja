package core.domain;

import core.exception.*;
import core.validator.BidValidator;
import dao.ProjectDAO;
import dao.UserDAO;

import static java.nio.charset.StandardCharsets.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class Project {
    private String title;
    private ArrayList<Skill> skills;
    private Integer budget;
    private String id;
    private String description;
    private String imageUrl;
    private long deadline;

    private long creationDate;

    // from auction
    private ArrayList<Bid> bids;
    private User winner;

    // ProjectDAO calls this ctor
    public Project(String title,
                   Integer budget,
                   String id,
                   String description,
                   String imageUrl,
                   long deadline,
                   long creationDate) {
        this.title = title;
        this.budget = budget;
        this.id = id;
        this.description = description;
        this.imageUrl = imageUrl;
        this.deadline = deadline;
        this.creationDate = creationDate;
        this.winner = null;
        this.bids = new ArrayList<>();
        this.skills = new ArrayList<>();
    }

    // end of from auction

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setDeadline(long deadline) {
        this.deadline = deadline;
    }

    public Project() {
        this.title = null;
        this.skills = new ArrayList<>();
        this.bids = new ArrayList<>();
        this.winner = null;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public Integer getBudget() {
        return budget;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public long getDeadline() {
        return deadline;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSkills(ArrayList<Skill> skills) {
        this.skills = skills;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void addSkill(Skill skill) {
        this.skills.add(skill);
    }


    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        String encodedProject;
        encodedProject =  "Project: {" +
                          "title='" + this.title + '\'' +
                          ", skills=";
        for (Skill skill: skills) {
            encodedProject += skill.toString();
        }
        encodedProject += ", budget=" + budget.toString();
        encodedProject += ", deadline=" + deadline;
        encodedProject += ", description=" + description;
        encodedProject += '}';
        return encodedProject;
    }

    public ArrayList<Bid> getBids() {
        return bids;
    }

    public void setBids(ArrayList<Bid> bids) {
        this.bids = bids;
    }

    public User getWinner() {
        return winner;
    }

    public void setWinner(User winner) {
        this.winner = winner;
    }
}
