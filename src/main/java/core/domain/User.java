package core.domain;

import java.util.ArrayList;

public class User {
    private String id;
    private String firstName, lastName;
    private ArrayList<Skill> skills;
    private String jobTitle;
    private String bio;
    private String imageUrl;
    private transient String password;

    public User() {
        this.skills = new ArrayList<Skill>();
    }

    public User(String userName) {
        this.skills = new ArrayList<Skill>();
    }

    public User(String userName, ArrayList<Skill> skills) {
        this.skills = skills;
    }

    public User(String id,
                String firstName,
                String lastName,
                String jobTitle,
                String bio,
                String imageUrl,
                String password) {

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.jobTitle = jobTitle;
        this.bio = bio;
        this.imageUrl = imageUrl;
        this.password = password;
        this.skills = new ArrayList<>();
    }

    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setSkills(ArrayList<Skill> skills) {
        this.skills = skills;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addSkill(Skill skill) {
        this.skills.add(skill);
    }

    public void removeSkill(String skillName) {
        for (int i = 0; i < skills.size(); i++) {
            if (skillName.equals(skills.get(i).getName())) {
                skills.remove(i);
                return;
            }
        }
    }

    public void endorseSkill(String skillName) {
        for (Skill skill: skills) {
            if (skill.getName().equals(skillName)) {
                skill.incrementPoint();
            }
        }
    }

    public Skill getSkill(String skillName) {
        for (Skill skill: skills) {
            if (skill.getName().equals((skillName))) {
                return skill;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String encodedUser;
        encodedUser =  "User: {" +
                       "id='" + id + '\'' +
                       ", skills=";
        for (Skill skill: skills) {
            encodedUser += skill.toString();
        }
        encodedUser += '}';
        return encodedUser;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getBio() {
        return bio;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
