package core.validator;

import core.domain.Bid;
import core.domain.Project;
import core.domain.Skill;

import java.util.ArrayList;

public class BidValidator {
    public static boolean qualifyUserForProject(ArrayList<Skill> userSkills, ArrayList<Skill> minReqSkills) {
        for (Skill reqSkill: minReqSkills) {
            boolean userDoesntHaveThisSkill = true;
            for (Skill skill : userSkills) {
                if (reqSkill.getName().equals(skill.getName())) {
                    userDoesntHaveThisSkill = false;
                    if (reqSkill.getPoint() > skill.getPoint()) {
                        return false;
                    } else {
                        break;
                    }
                }
            }
            if (userDoesntHaveThisSkill) {
                return false;
            }
        }
        return true;
    }

    public static boolean qualifyBidAmountForProject(Bid bid, Project project) {
        return bid.getBidAmount() <= project.getBudget();
    }
}
