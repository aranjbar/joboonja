package utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import core.domain.Project;
import dao.ProjectDAO;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class RecentProjectsPusher implements Runnable {

    public void run() {
        String mostRecentProjectCreationDateQuery = "select max(creationDate) as maxmax from projects";
        Long mostRecentBlahBlahBlah = 0L;

        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(mostRecentProjectCreationDateQuery);

            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                mostRecentBlahBlahBlah = rs.getLong("maxmax");
            }

            pstmt.close();
            con.close();

            try {
                String projectsListString = utils.Request.sendGetRequest("http://142.93.134.194:8000/joboonja/project");

                // the actual code
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<Project>>(){}.getType();
                try {
                    ArrayList<Project> projectList =
                            gson.fromJson(new InputStreamReader(new ByteArrayInputStream(projectsListString.getBytes()), "UTF-8"), listType);
                    for (Project p:projectList) {
                        if (p.getCreationDate() > mostRecentBlahBlahBlah) {
                            ProjectDAO.addProject(p);
                        }
                    }
                } catch (UnsupportedEncodingException e) {}

            } catch (Exception e) {
                System.out.println("RecentProjectPusher: can not fetch projects from remote server (how gittish!!!)");
            }

        } catch (SQLException e) {
            System.out.println("Error in RecentProjectPusher");
        }
        System.out.println("hello");
    }

}
