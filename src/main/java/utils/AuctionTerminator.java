package utils;

import controller.project.ProjectCtrl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuctionTerminator implements Runnable {

    public void run() {
        Long now = System.currentTimeMillis();

        // no worries about sql injection
        String openProjectsQuery = "select id from projects where " + now + " - deadline < 120000";

        try {
            Connection con = ConnectionPool.getConnection();
            PreparedStatement pstmt = con.prepareStatement(openProjectsQuery);

            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                String _projectId = rs.getString("id");
                ProjectCtrl.terminateProjectAuction(_projectId, now);
            }

            rs.close();
            pstmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println("Error in AuctionTerminator::run");
        }


    }


}
