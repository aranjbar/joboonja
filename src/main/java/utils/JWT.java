package utils;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.security.Key;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import core.domain.User;
import dao.UserDAO;
import io.github.cdimascio.dotenv.Dotenv;
import io.jsonwebtoken.*;

import java.util.Date;
import java.util.LinkedHashMap;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class JWT {

    // The secret key. This should be in a property file NOT under source
    // control and not hard coded in real life. We're putting it here for
    // simplicity.
//    private static Dotenv dotenv = Dotenv.load();

    private static String SECRET_KEY = System.getenv("JWT_SECRET_KEY"); // dotenv.get("JWT_SECRET_KEY");

    // Sample method to construct a JWT
    public static String createJWT(User user, long ttlMillis) {
        // The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        // We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        // Let's set the JWT Claims
        Gson gson = new Gson();
        Type userType = new TypeToken<User>(){}.getType();
        String userJson = gson.toJson(user, userType);
        JwtBuilder builder = Jwts.builder().claim("user", userJson)
                .setIssuedAt(now)
                .signWith(signatureAlgorithm, signingKey);

        // If it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        // Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();

    }

    public static Claims decodeJWT(String token) {
        String jwt = token.split(" ")[1];
        // This line will throw an exception if it is not a signed JWS (as expected)
        return Jwts.parser()
                .setSigningKey((DatatypeConverter.parseBase64Binary(SECRET_KEY)))
                .parseClaimsJws(jwt).getBody();
    }

    public static Boolean isValidToken(String token) {
        Claims claims = JWT.decodeJWT(token);
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        return claims.getExpiration().after(now);
    }

    public static User getPayload(String token) {
        Claims claims = JWT.decodeJWT(token);
        String userJson = claims.get("user", String.class);
        Gson gson = new Gson();
        Type userType = new TypeToken<User>(){}.getType();
        User u = gson.fromJson(userJson, userType);
        return UserDAO.retrieveUserByID(u.getId());
    }
}
