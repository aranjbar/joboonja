package utils;

import com.zaxxer.hikari.*;
import java.sql.*;


//public class ConnectionPool  {
//    private static BasicDataSource ds;
//
//    private static void createBasicDataSource() {
//        if (ds == null) {
//
//            BasicDataSource bds = new BasicDataSource();
//            bds.setUrl("jdbc:sqlite:C:/Users/Rouhollah/joboonja/joboonja.db");
//            bds.setMinIdle(5);
//            bds.setMaxIdle(10);
//            bds.setMaxOpenPreparedStatements(100);
//
//            ds = bds;
//        }
//    }
//
//    public static Connection getConnection() throws SQLException {
////        createBasicDataSource();
////        return ds.getConnection();
//
//        try (Connection conn = DriverManager.getConnection("jdbc:sqlite:C:/Users/Rouhollah/joboonja/joboonja.db")) {
//            if (conn != null) {
//                return conn;
//            }
//
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//
//        return null;
//
//    }
//
//    private ConnectionPool() {}
//
//}



public class ConnectionPool {

    private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;

    static {
        config.setPoolName("MySQLPool");
        config.setDriverClassName("com.mysql.cj.jdbc.Driver");
        config.setJdbcUrl("jdbc:mysql://localhost:3306/joboonja?useSSL=false");
        config.setUsername("root");
        config.setPassword("example");
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.setMaxLifetime(60000); // 60 Sec
        config.setIdleTimeout(45000); // 45 Sec
        config.setMaximumPoolSize(50); // 50 Connections (including idle connections)
        ds = new HikariDataSource(config);
    }

    private ConnectionPool() {}

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}