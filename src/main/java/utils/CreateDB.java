package utils;

import utils.ConnectionPool;

import javax.swing.plaf.nimbus.State;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateDB {
    public static void main(String[] argv) {
        try {
            // if the db file does not exist, it automatically creates the db
            Connection con = ConnectionPool.getConnection();
            System.out.println("Database successfully created");

            String createProjectsTable = "CREATE TABLE if not exists projects " +
                    "(id VARCHAR(255) NOT NULL," +
                    " title TEXT NOT NULL," +
                    " description TEXT NOT NULL," +
                    " imageUrl TEXT," +
                    " budget INTEGER NOT NULL," +
                    " deadline BIGINT NOT NULL," +
                    " creationDate BIGINT NOT NULL," +
                    " PRIMARY KEY ( id ))";


            String createUsersTable = "create table if not exists users" +
                    "(id varchar(255) NOT NULL," +
                    "firstName text NOT NULL," +
                    "lastName text NOT NULL," +
                    "jobTitle text," +
                    "bio text," +
                    "imageUrl text," +
                    "password text NOT NULL," +
                    "primary key (id))";

            String createSkillsTable = "create table if not exists skills" +
                    "(name varchar(255) NOT NULL," +
                    "primary key (name))";


            String createEndorsesTable = "create table if not exists endorses" +
                    "(endorserId varchar(255) NOT NULL," +
                    "endorsedId varchar(255) NOT NULL," +
                    "skillName varchar(255) NOT NULL," +
                    "foreign key (endorserId) references users(id)," +
                    "foreign key (endorsedId) references users(id)," +
                    "foreign key (skillName) references skills(name))";

            String createBidsTable = "create table if not exists bids" +
                    "(bidderId varchar(255) NOT NULL," +
                    "projectId varchar(255) NOT NULL," +
                    "amount integer NOT NULL," +
                    "primary key (bidderId, projectId), " +
                    "foreign key (bidderId) references users(id)," +
                    "foreign key (projectId) references projects(id))";

            String createWinnerUsersTable = "create table if not exists winnerUsers" +
                    "(winnerId varchar(255) NOT NULL," +
                    "projectId varchar(255) NOT NULL," +
                    "foreign key (winnerId) references users(id)," +
                    "foreign key (projectId) references projects(id))";

            String createUserSkillsTable = "create table if not exists userSkills" +
                    "(userId varchar(255) NOT NULL," +
                    "skillName varchar(255) NOT NULL," +
                    "point integer NOT NULL," +
                    "foreign key (userId) references users(id)," +
                    "foreign key(skillName) references skills(name))";

            String createProjectSkillsTable = "create table if not exists projectSkills" +
                    "(projectId varchar(255) NOT NULL," +
                    "skillName varchar(255) NOT NULL," +
                    "point integer NOT NULL," +
                    "foreign key (projectId) references projects(id)," +
                    "foreign key(skillName) references skills(name))";

            PreparedStatement pstmt = con.prepareStatement(createProjectsTable);
            pstmt.executeUpdate();

            pstmt = con.prepareStatement(createUsersTable);
            pstmt.executeUpdate();

            pstmt = con.prepareStatement(createSkillsTable);
            pstmt.executeUpdate();

            pstmt = con.prepareStatement(createEndorsesTable);
            pstmt.executeUpdate();

            pstmt = con.prepareStatement(createBidsTable);
            pstmt.executeUpdate();

            pstmt = con.prepareStatement(createWinnerUsersTable);
            pstmt.executeUpdate();

            pstmt = con.prepareStatement(createUserSkillsTable);
            pstmt.executeUpdate();

            pstmt = con.prepareStatement(createProjectSkillsTable);
            pstmt.executeUpdate();


            System.out.println("done");


        } catch (SQLException e) {
            System.out.println("Error getting connection");
        }
    }
}
