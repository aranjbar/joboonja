package controller.user;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import controller.authentication.AuthCtrl;
import core.domain.Skill;
import core.domain.User;
import core.exception.EndorseAlreadyExists;
import dao.EndorseDAO;
import dao.SkillDAO;
import dao.UserDAO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.JWT;
import utils.PBKDF2;

import java.lang.reflect.Type;
import java.util.ArrayList;

@RestController
public class UserCtrl {
    ExclusionStrategy userExclusionStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }

        @Override
        public boolean shouldSkipField(FieldAttributes field) {
            String fieldName = field.getName();
            return fieldName.equals("lastName") || fieldName.equals("skills") || fieldName.equals("bio");
        }
    };

    private static class UserPass {
        String username;
        String password;

        public String getUsername() { return username; }
        public void setUsername(String username) { this.username = username; }
        public String getPassword() { return password; }
        public void setPassword(String password) { this.password = password; }
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET, produces = "application/json;charset=utf8")
    public ResponseEntity<String> getUserList(
            @RequestAttribute("user") User user) {
        try {
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }

            ArrayList<User> userList = new ArrayList<>(UserDAO.retrieveOtherUsersList(user.getId()));

            Gson listGson = new GsonBuilder()
                    .addSerializationExclusionStrategy(userExclusionStrategy)
                    .create();
            Type userListType = new TypeToken<ArrayList<User>>(){}.getType();
            String userListJson = listGson.toJson(userList, userListType);
            return ResponseEntity.status(HttpStatus.OK).body(userListJson);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @RequestMapping(value = "/users/register", method = RequestMethod.POST, produces = "application/json;charset=utf8")
    public ResponseEntity<String> registerNewUser(
            @RequestBody User user
    ) {
        try {
            User u = UserDAO.retrieveUserByID(user.getId());
            if ( u != null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }
            System.out.println("pass: " + user.getPassword());
            String passwordHash = PBKDF2.generateHash(user.getPassword());
            user.setPassword(passwordHash);
            UserDAO.addUser(user);
            Gson gson = new Gson();
            Type userType = new TypeToken<User>(){}.getType();
            String userJson = gson.toJson(user, userType);
            return ResponseEntity.status(HttpStatus.OK).body(userJson);
        } catch (Exception e) {
            System.out.printf(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

    @RequestMapping(value = "/users/authenticate", method = RequestMethod.POST, produces = "application/json;charset=utf8")
    public ResponseEntity<String> usersAuthenticate(
            @RequestBody UserCtrl.UserPass userPass) {
        try {
            User login_user = UserDAO.retrieveUserByID(userPass.getUsername());
            if (login_user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            if (PBKDF2.validatePassword(userPass.getPassword(), login_user.getPassword())) {
                String jwt = JWT.createJWT(login_user, 86400000);
                return ResponseEntity.status(HttpStatus.OK).body(jwt);
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET, produces = "application/json;charset=utf8")
    public ResponseEntity<String> getAuthenticatedUser(
            @RequestAttribute("user") User user
    ) {
        try {
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            Gson gson = new Gson();
            Type userType = new TypeToken<User>(){}.getType();
            String userJson = gson.toJson(user, userType);
            return ResponseEntity.status(HttpStatus.OK).body(userJson);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @RequestMapping(value = "/users/{username}", method = RequestMethod.GET, produces = "application/json;charset=utf8")
    public ResponseEntity<String> getSingeUser(
            @RequestAttribute("user") User u,
            @PathVariable("username") String username) {
        try {
            User user = UserDAO.retrieveUserByID(u.getId());
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            Gson gson = new Gson();
            Type userType = new TypeToken<User>(){}.getType();
            if (user.getId().equals(username)) {
                String userJson = gson.toJson(user, userType);
                return ResponseEntity.status(HttpStatus.OK).body(userJson);
            } else {
                User otherUser = UserDAO.retrieveUserByID(username);
                String otherUserJson = gson.toJson(otherUser, userType);
                return ResponseEntity.status(HttpStatus.OK).body(otherUserJson);
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @RequestMapping(value = "/users/{username}/skill/endorse", method = RequestMethod.POST)
    public ResponseEntity<Skill> endorseUserSkill(
            @RequestAttribute("user") User endorserUser,
            @PathVariable("username") String username,
            @RequestBody Skill skill) {
        try {
            if (endorserUser == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            if (username.equals(endorserUser.getId())) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
            }

            User endorsedUser = UserDAO.retrieveUserByID(username);
            if (endorsedUser == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }

            String endorserUserId = endorserUser.getId();
            String endorsedUserId = endorsedUser.getId();
            EndorseDAO.addEndorse(endorserUserId, endorsedUserId, skill.getName());
            UserDAO.retrieveUserByID(endorsedUserId).endorseSkill(skill.getName());
            return ResponseEntity.status(HttpStatus.OK).body(endorsedUser.getSkill(skill.getName()));
        }catch (EndorseAlreadyExists e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @RequestMapping(value = "/users/{username}/skill/endorsed", method = RequestMethod.GET, produces = "application/json;charset=utf8")
    public ResponseEntity<ArrayList<String>> getEndorsedSkills(
            @RequestAttribute("user") User endorserUser,
            @PathVariable("username") String username,
            @RequestHeader("endorser") String endorser
    ) {
        try {
            if (endorserUser == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            if (username.equals(endorserUser.getId())) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
            }

            User endorsedUser = UserDAO.retrieveUserByID(username);
            if (endorsedUser == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            String endorserUserId = endorserUser.getId();
            String endorsedUserId = endorsedUser.getId();
            ArrayList<String> endorsedSkills = EndorseDAO.retrieveUserEndorses(endorserUserId, endorsedUserId);
            return ResponseEntity.status(HttpStatus.OK).body(endorsedSkills);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @RequestMapping(value = "/users/{username}/skill", method = RequestMethod.POST)
    public ResponseEntity<Skill> addUserSkill(
            @RequestAttribute("user") User user,
            @PathVariable("username") String username,
            @RequestBody Skill skill) {
        try {
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            if (!username.equals(user.getId())) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
            }

            skill.setPoint(0);
            ArrayList<Skill> availableSkillList = SkillDAO.subtractSkillArray(user.getSkills());
            if (skill.in(availableSkillList)) {
                UserDAO.addSkill(user.getId(), skill);
                return ResponseEntity.status(HttpStatus.OK).body(skill);
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @RequestMapping(value = "/users/{username}/skill", method = RequestMethod.DELETE)
    public ResponseEntity<Skill> deleteUserSkill(
            @RequestAttribute("user") User user,
            @PathVariable("username") String username,
            @RequestBody Skill skill
    ) {
        try {
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            if (!username.equals(user.getId())) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
            }

            skill = user.getSkill(skill.getName());
            if (skill == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            } else {
                user.removeSkill(skill.getName());
                return ResponseEntity.status(HttpStatus.OK).body(skill);
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }
}
