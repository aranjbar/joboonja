package controller.skill;

import controller.authentication.AuthCtrl;
import core.domain.Skill;
import core.domain.User;
import dao.SkillDAO;
import dao.UserDAO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.JWT;

import java.util.ArrayList;

@RestController
public class SkillCtrl {
    @RequestMapping(value = "/skills/available", method = RequestMethod.GET)
    public ResponseEntity<ArrayList<Skill>> getAvailableSkillsForUser(
            @RequestAttribute("user") User user)
    {
        try {
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            ArrayList<Skill> availableSkills = SkillDAO.subtractSkillArray(user.getSkills());
            return ResponseEntity.status(HttpStatus.OK).body(availableSkills);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }
}
