package controller.search;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import core.domain.Project;
import core.domain.User;
import dao.ProjectDAO;
import dao.UserDAO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.JWT;

import java.lang.reflect.Type;
import java.util.ArrayList;


@RestController
public class SearchCtrl {
    @RequestMapping(value = "/search/user", method = RequestMethod.GET, produces = "application/json;charset=utf8")
    public ResponseEntity<String> searchUser(
            @RequestAttribute("user") User user,
            @RequestParam String searchQuery
    ) {
        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        ArrayList<User> userList = UserDAO.searchUser("%" + searchQuery + "%");

        Gson listGson = new Gson();
        Type projectListType = new TypeToken<ArrayList<User>>(){}.getType();
        String userListJson = listGson.toJson(userList, projectListType);
        return ResponseEntity.status(HttpStatus.OK).body(userListJson);
    }


    @RequestMapping(value = "/search/project", method = RequestMethod.GET, produces = "application/json;charset=utf8")
    public ResponseEntity<String> searchProject(
            @RequestAttribute("user") User user,
            @RequestParam String searchQuery
    ) {
        if (user == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        ArrayList<Project> projectList = ProjectDAO.searchProject("%" + searchQuery + "%");

        Gson listGson = new Gson();
        Type projectListType = new TypeToken<ArrayList<User>>(){}.getType();
        String projectListJson = listGson.toJson(projectList, projectListType);
        return ResponseEntity.status(HttpStatus.OK).body(projectListJson);
    }
}