package controller;

import core.domain.Skill;
import core.domain.User;
import dao.ProjectDAO;
import dao.SkillDAO;
import dao.UserDAO;
import utils.AuctionTerminator;
import utils.PBKDF2;
import utils.RecentProjectsPusher;
//import web_server.WebServer;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@WebListener
public class WebServerInitializer implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("Initializing database from the data server");
        getInitialData();
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

        scheduler.scheduleAtFixedRate(new RecentProjectsPusher(), 5, 5, TimeUnit.MINUTES);
        System.out.println("RecentProjectsPusher is now running");

        scheduler.scheduleAtFixedRate(new AuctionTerminator(), 0, 1, TimeUnit.MINUTES);
        System.out.println("AuctionTerminator is now running");

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("Servlet has been stopped.");
    }

    private void getInitialData() {
        try {

            String skillsListString = utils.Request.sendGetRequest("http://142.93.134.194:8000/joboonja/skill");
            SkillDAO.addSkillBulk(skillsListString);

            User user = new User();
            user.setId("1");
            user.setFirstName("علی");
            user.setLastName("شریف زاده");
            user.setJobTitle("برنامه‌نویس وب");
            user.setBio("با توجه به تجربه بالا در زمینه برنامه نویسی, فراگیری هر زبانی تا سطح حرفه ای در کمتر از یک هفته برای اینجانب مقدور میباشد. این روزها در اتحادیه اینترنت مقرون به صرفه که یک موسسه غیر انتفاعی است و از سوی گوگل، فیسبوک و مایکروسافت حمایت می\u200Cشود کار می کنم. این موسسه در تلاش است تا دسترسی به اینترنت پر سرعت را برای همه در دنیا ممکن کند.");
            user.addSkill(new Skill("HTML", 5));
            user.addSkill(new Skill("Javascript", 4));
            user.addSkill(new Skill("C++", 0));
            user.addSkill(new Skill("Java", 3));
            user.addSkill(new Skill("CSS", 5));
            user.addSkill(new Skill("PHP", 6));
            user.setImageUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Anonymous_emblem.svg/1200px-Anonymous_emblem.svg.png");
            user.setPassword(PBKDF2.generateHash("test"));
            UserDAO.addUser(user);


            User user1 = new User();
            user1.setId("aranjbar");
            user1.setFirstName("علی");
            user1.setLastName("رنجبر");
            user1.setJobTitle("آی‌در دار");
            user1.setBio("بی‌گمان موسم رسیدن اوست.");
            user1.addSkill(new Skill("HTML", 5));
            user1.addSkill(new Skill("Javascript", 4));
            user1.addSkill(new Skill("C++", 2));
            user1.addSkill(new Skill("Java", 3));
            user1.setPassword(PBKDF2.generateHash("aranjbar"));
            UserDAO.addUser(user1);


            User user2 = new User();
            user2.setId("rooholah");
            user2.setFirstName("روح اله");
            user2.setLastName("ابوالحسنی");
            user2.setJobTitle("فریلنسر");
            user2.setBio("در حال تحویل پروژه آی ای");
            user2.addSkill(new Skill("HTML", 5));
            user2.addSkill(new Skill("Javascript", 4));
            user2.addSkill(new Skill("C++", 2));
            user2.addSkill(new Skill("Java", 3));
            user2.setPassword(PBKDF2.generateHash("rooholah"));
            UserDAO.addUser(user2);


            String projectsListString = utils.Request.sendGetRequest("http://142.93.134.194:8000/joboonja/project");
            ProjectDAO.addProjectBulk(projectsListString);

        } catch (Exception e) {
            System.out.println("Can't get skills list from the server");
        }
    }

}