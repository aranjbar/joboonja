package controller.authentication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import controller.exception.UnauthorizedAccess;
import core.domain.User;
import dao.UserDAO;
import io.jsonwebtoken.Claims;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.JWT;
import utils.PBKDF2;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Date;

@RestController
public class AuthCtrl {
    @RequestMapping(value = "/verifytoken", method = RequestMethod.GET)
    public ResponseEntity verifyToken(
            @RequestHeader("Authorization") String token
    ) {
        if (JWT.isValidToken(token)) {
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }
}
