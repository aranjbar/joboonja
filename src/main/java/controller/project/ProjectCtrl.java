package controller.project;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import core.domain.Bid;
import core.domain.Project;
import core.domain.Skill;
import core.domain.User;
import core.exception.HighBidAmount;
import core.validator.BidValidator;
import dao.ProjectDAO;
import dao.UserDAO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.util.ArrayList;

@RestController
public class ProjectCtrl {
    @RequestMapping(value = "/projects", method = RequestMethod.GET, produces = "application/json;charset=utf8")
    public ResponseEntity<String> getProjectList(
            @RequestAttribute("user") User user,
            @RequestParam Integer itemsPerPage,
            @RequestParam Integer pageNumber
            ) {

        try {
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            ArrayList<Project> projectList = ProjectDAO.retrieveAllProjects(itemsPerPage, pageNumber);

            Gson listGson = new Gson();
            Type projectListType = new TypeToken<ArrayList<Project>>(){}.getType();
            String projectListJson = listGson.toJson(projectList, projectListType);
            return ResponseEntity.status(HttpStatus.OK).body(projectListJson);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @RequestMapping(value = "/projects/{id}", method = RequestMethod.GET, produces = "application/json;charset=utf8")
    public ResponseEntity<Project> getProject(
            @RequestAttribute("user") User user,
            @PathVariable("id") String projectId) {
        try {
            Project project = ProjectDAO.retrieveProjectById(projectId);

            if (user == null || project == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }

            if (!BidValidator.qualifyUserForProject(user.getSkills(), project.getSkills())) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
            }

            return ResponseEntity.status(HttpStatus.OK).body(project);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        }
    }

    @RequestMapping(value = "/projects/{id}/bid", method = RequestMethod.POST, produces = "application/json;charset=utf8")
    public ResponseEntity<Bid> bidProject(
            @RequestAttribute("user") User biddingUser,
            @PathVariable("id") String projectId,
            @RequestBody Bid bidAmount) {


        try {
            Project project = ProjectDAO.retrieveProjectById(projectId);
            if (biddingUser == null || project == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }

            if (!BidValidator.qualifyUserForProject(biddingUser.getSkills(), project.getSkills()) ||
                    ProjectDAO.retrieveBid(biddingUser.getId(), projectId) != null) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
            }

            Bid bid = new Bid();
            bid.setBiddingUserId(biddingUser.getId());
            bid.setBidAmount(bidAmount.getBidAmount());
            bid.setProjectId(projectId);

            if (!BidValidator.qualifyBidAmountForProject(bid, project)) {
                throw new HighBidAmount();
            }

            ProjectDAO.insertBid(bid);
            System.out.println("ooohooo");

            return ResponseEntity.status(HttpStatus.OK).body(bid);
        } catch(Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
    }


    // this method is somehow an entry point to the system
    public static void terminateProjectAuction(String projectId, Long currentTime) {
        Project project = ProjectDAO.retrieveProjectById(projectId);
        if (project == null) {
            System.out.println("There is no such project");
            return;
        }

        // if the deadline is past more than 2 minutes from the time the whole process commenced,
        // the auction is probably handled earlier. this code is prone to failure
        if (currentTime - project.getDeadline() > 120000) {
            System.out.println("The auction has been terminated.");
            return;
        }
        ArrayList<Skill> projectSkills = project.getSkills();

        Double bestValue = -1.0;
        String bestUserName = null;

        for (Bid bid: project.getBids()) {
            ArrayList<Skill> userSkills = UserDAO.retrieveUserByID(bid.getBiddingUserId()).getSkills();
            Double userValue = 0.0;
            for(Skill reqSkill: projectSkills) {
                for(Skill userSkill: userSkills) {
                    if (reqSkill.getName().equals(userSkill.getName())) {
                        userValue += 10000 * Math.pow((double) (userSkill.getPoint() - reqSkill.getPoint()), 2);
                    }
                }
            }
            userValue += project.getBudget() - bid.getBidAmount();

            if (userValue > bestValue) {
                bestValue = userValue;
                bestUserName = bid.getBiddingUserId();
            }
        }

        ProjectDAO.terminateProjectAuctionById(projectId, bestUserName);

    }

}