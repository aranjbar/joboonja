FROM tomcat:9.0

COPY mysql-connector-java-8.0.15.jar /usr/local/tomcat/lib/
EXPOSE 8080
CMD ["catalina.sh", "run" ]